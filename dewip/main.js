var app = window.angular.module('DewipDemo', ['ngMaterial']);

var _ = window._;

app.controller('MainCtrl', [
  '$scope', '$mdSidenav', '$location', '$anchorScroll',
  function($scope, $mdSideNav, $location, $anchorScroll) {

    var main = this;
    $scope.toggleSideNav = function(menuId) {
      $mdSideNav(menuId).toggle();
    };
    $scope.scrollTo = function(id) {
      $location.hash(id);
      $anchorScroll();
    };

    main.itemsInput = '';

    $scope.$watch('main.itemsInput', function(value) {
      main.items = main.itemsInput.split(',').map(Function.prototype.call, ''.trim);
    });
    main.itemCount = function(itemsInput) {
      return itemsInput.split(',').length;
    };

    main.machinesInput = '';

    $scope.$watch('main.machinesInput', function(value) {
      main.machines = main.machinesInput.split(',').map(Function.prototype.call, ''.trim);
    });

    main.currentTime = 0;
    main.dummyData = function() {
      main.machines = [
        'Torno',
        'Fresa',
        'Lixa'
      ];
      main.machinesInput = main.machines.join(', ');

      main.items = [
        'Item A',
        'Item B',
        'Item C',
        'Item D'
      ];
      main.itemsInput = main.items.join(', ');
      main.maxWip = 2;

      main.leadtimes = {
        'Item A': {
          Torno: 10,
          Fresa: 5,
          Lixa: 13
        },
        'Item B': {
          Torno: 15,
          Lixa: 10
        },
        'Item C': {
          Fresa: 9
        },
        'Item D': {
          Fresa: 8,
          Torno: 11
        }
      };
    };

    main.currentWip = [ ];

    var _itemsMovingForward = function(t, wip) {
      return _.filter(wip, function(item) {
        return item.until <= t || item.until === void 0;
      });
    };

    var _cleanUp = function(t, wip) {
      return _.filter(wip, function(item) {
        var hasMore = item.visited.length < _.values(_.filter(main.leadtimes[item.item], x => (x > 0))).length;
        if (item.until <= t && !hasMore) {
          var slot = main.complete[item.item];
          if (!main.complete[item.item]) {
            main.complete[item.item] = 1;
          }
          else {
            ++main.complete[item.item];
          }
          return false;
        }
        return true;
      }).map(function(item) {
        if (item.until <= t)
          item.until = void 0;
        return item;
      });
    };

    var _availableMachines = function(t, wip) {
      if (!wip.length) return main.machines;
      
      return _.uniq(_.pluck(wip.filter(function(item) {
        return item.until <= t;
      }), 'visited').map(function(val) {
        return _.last(val);
      }));
    };

    var _willProcess = function(item, machine) {
      var shouldProcess = main.leadtimes[item.item][machine] > 0,
          hasProcessed = item.visited.indexOf(machine) !== -1;

      return shouldProcess && !hasProcessed;
    };

    var _wipCount = function(wip) {
      return _.countBy(wip, (item => item.item));
    };

    var _update = function() {
      main.processing || (main.processing = { });
      main.complete || (main.complete = { });

      var movingOn = _itemsMovingForward(main.currentTime, main.currentWip);
      var availableMachines = _availableMachines(main.currentTime, main.currentWip);

      main.currentWip = _cleanUp(main.currentTime, main.currentWip);
      availableMachines.forEach(function(machine) {
        var wipCount = _wipCount(main.currentWip);
        var allowedItems = movingOn.filter(function(wipItem) {
            return _willProcess(wipItem, machine);
        });
        var item;
        if (allowedItems.length) {
          // Remove o primeiro
          item = allowedItems[0];
          movingOn = _.drop(movingOn);

          // atualiza o WIP
          item.visited.push(machine);
          item.until = main.currentTime + main.leadtimes[item.item][machine];
          main.processing[machine] = item.item;
        }
        else {
          allowedItems = main.items.filter(function(item) {
            return (wipCount[item] || 0) < main.maxWip &&
              main.leadtimes[item][machine] > 0;
          });
          if (allowedItems.length) {
            item = allowedItems[0];
            main.currentWip.push({
              item: item,
              until: main.currentTime + main.leadtimes[item][machine],
              visited: [machine]
            });
            main.processing[machine] = item;
          }
        }
      });
    };

    main.goForward = function() {
      main.currentTime = main.nextEvent || _.min(_.flatten(_.values(main.leadtimes).map(_.values)));

      // Atualiza qual máquina processa qual peça
      _update();
      main.wipCount = _wipCount(main.currentWip);

      main.nextEvent = _.min(main.currentWip.map(function(item) {
        if (item.until <= main.currentTime)
          return +Infinity;
        return item.until;
      }));
    };
  }]);
